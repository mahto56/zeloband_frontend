import React from "react";
import "./Login.css";
import StyledLink from "../../utils/StyledLink";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { loginAction } from "../../../actions/loginAction";
import { Redirect } from "react-router-dom";
class Login extends React.Component {
  submit = values => {
    // console.log(values);
    this.props.loginAction(values, this.props.history);
  };

  errorMessage() {
    if (this.props.errorMessage) {
      return <div className="info-red">{this.props.errorMessage}</div>;
    }
  }

  render() {
    const { handleSubmit } = this.props;
    if (this.props.authenticated) return <Redirect to="/dashboard" />;

    return (
      <div className="login-block">
        <div className="login-card">
          <h2 className="login__title">Login</h2>

          <form
            onSubmit={handleSubmit(this.submit)}
            className="login-input-grp"
          >
            <div>
              <Field
                component="input"
                className="login-ctrl"
                type="text"
                placeholder="Email"
                name="email"
              />
            </div>
            <div>
              <Field
                component="input"
                className="login-ctrl"
                type="password"
                placeholder="Password"
                name="password"
              />
            </div>
            <button type="submit" style={{}} className="login__choose--btn">
              Login
            </button>
          </form>
          {this.errorMessage()}
          <div className="signin--block">
            Forgot your password?
            <StyledLink
              to="/reset"
              style={{
                color: "#00f",
                display: "inline",
                margin: "0 20px",
                height: "",

                textDecoration: "underline"
              }}
            >
              Reset
            </StyledLink>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  errorMessage: state.auth.error,
  authenticated: state.auth.authenticated,
  user_data: state.auth.user_data
});

const reduxFormLogin = reduxForm({
  form: "login"
})(Login);

export default connect(
  mapStateToProps,
  { loginAction }
)(reduxFormLogin);
